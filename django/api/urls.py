from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateAuthorView, DetailsAuthorView, CreateBlogPostView, DetailsBlogPostView

urlpatterns = {
    path('authors/', CreateAuthorView.as_view(), name="create_author"),
    path('authors/<int:pk>/', DetailsAuthorView.as_view(), name="details_author"),
    path('posts/', CreateBlogPostView.as_view(), name="create_post"),
    path('posts/<int:pk>/', DetailsBlogPostView.as_view(), name="details_post"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
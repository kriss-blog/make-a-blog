class CreateBlogPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.string :summary
      t.string :body
      t.string :tags, array: true, default: []
      t.references :author, foreign_key: true

      t.timestamps
    end
  end
end
